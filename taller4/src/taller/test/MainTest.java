package taller.test;

import junit.framework.TestCase;
import taller.mundo.AlgorithmTournament.TipoOrdenamiento;
import taller.mundo.teams.BubbleSortTeam;
import taller.mundo.teams.InsertionSortTeam;
import taller.mundo.teams.MergeSortTeam;
import taller.mundo.teams.QuickSortTeam;
import taller.mundo.teams.SelectionSortTeam;

public class MainTest extends TestCase
{	
	@SuppressWarnings("rawtypes")
	Comparable[] lista = {1,3,4,2};
	@SuppressWarnings("unchecked")
	public void testBubujaSort()
	{
		BubbleSortTeam burbuja = new BubbleSortTeam();

		burbuja.sort(lista, TipoOrdenamiento.ASCENDENTE);
		for (int i = 0; i < lista.length-1; i++) 
		{
			assertTrue(lista[i].compareTo(lista[i+1])<=0);
		}
		burbuja.sort(lista, TipoOrdenamiento.DESCENDENTE);
		for (int i = 0; i < lista.length-1; i++) 
		{
			assertTrue(lista[i].compareTo(lista[i+1])>=0);
		}
	}
	@SuppressWarnings("unchecked")
	public void testInsertionSort()
	{
		InsertionSortTeam insertion = new InsertionSortTeam();
		insertion.sort(lista, TipoOrdenamiento.ASCENDENTE);
		for (int i = 0; i < lista.length-1; i++) 
		{
			assertTrue(lista[i].compareTo(lista[i+1])<=0);
		}
		insertion.sort(lista, TipoOrdenamiento.DESCENDENTE);
		for (int i = 0; i < lista.length-1; i++) 
		{
			assertTrue(lista[i].compareTo(lista[i+1])>=0);
		}	
	}
	@SuppressWarnings("unchecked")
	public void testSelectionSort()
	{
		SelectionSortTeam selection = new SelectionSortTeam();
		selection.sort(lista, TipoOrdenamiento.ASCENDENTE);
		for (int i = 0; i < lista.length-1; i++) 
		{
			assertTrue(lista[i].compareTo(lista[i+1])<=0);
		}
		selection.sort(lista, TipoOrdenamiento.DESCENDENTE);
		for (int i = 0; i < lista.length-1; i++) 
		{
			assertTrue(lista[i].compareTo(lista[i+1])>=0);
		}
	}
	public void testQuickSort()
	{
		QuickSortTeam quick = new QuickSortTeam();
		quick.sort(lista, TipoOrdenamiento.ASCENDENTE);
		for (int i = 0; i < lista.length-1; i++) 
		{
			assertTrue(lista[i].compareTo(lista[i+1])<=0);
		}
		quick.sort(lista, TipoOrdenamiento.DESCENDENTE);
		for (int i = 0; i < lista.length-1; i++) 
		{
			assertTrue(lista[i].compareTo(lista[i+1])>=0);
		}
	}
	public void testMergeSort()
	{
		MergeSortTeam merge = new MergeSortTeam();
		merge.sort(lista, TipoOrdenamiento.ASCENDENTE);
		for (int i = 0; i < lista.length-1; i++) 
		{
			assertTrue(lista[i].compareTo(lista[i+1])<=0);
		}
		merge.sort(lista, TipoOrdenamiento.DESCENDENTE);
		for (int i = 0; i < lista.length-1; i++) 
		{
			assertTrue(lista[i].compareTo(lista[i+1])>=0);
		}
	}


}
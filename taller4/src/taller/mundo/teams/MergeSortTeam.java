package taller.mundo.teams;

/*
 * MergeSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;
import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class MergeSortTeam extends AlgorithmTeam
{
	public MergeSortTeam()
	{
		super("Merge sort (*)");
		userDefined = true;
	}

	@Override
	public Comparable[] sort(Comparable[] lista, TipoOrdenamiento orden)
	{
		return merge_sort(lista, orden);
	}


	private static Comparable[] merge_sort(Comparable[] lista, TipoOrdenamiento orden)
	{
		// Trabajo en Clase
		if( lista.length < 2 )
		{
			return lista;
		}
		int medio = lista.length/2;
		Comparable[] izq = new Comparable[medio];
		Comparable[] der = new Comparable[lista.length - medio];
		for (int i = 0; i < medio; i++)
		{
			izq[i] = lista[i];
		}
		for (int i = medio; i < lista.length; i++)
		{
			der[i-medio] = lista[i];
		}
		Comparable[] izquierda = merge_sort(izq, orden);
		Comparable[] derecha = merge_sort(der, orden);		
		return merge(izquierda, derecha, orden);
	}

	private static Comparable[] merge(Comparable[] izquierda, Comparable[] derecha, TipoOrdenamiento orden)
	{
		// Trabajo en Clase 
		int i = 0;
		int d = 0;
		int o = 0;
		Comparable[] lista = new Comparable[ izquierda.length + derecha.length ];
		while( i < izquierda.length && d < derecha.length )
		{
			if( orden == TipoOrdenamiento.ASCENDENTE  )
			{
				if(  izquierda[i].compareTo( derecha[d] ) < 0)
				{
					lista[o] = izquierda[i];
					i++;
				}
				else
				{
					lista[o] = derecha[d];
					d++;
				}
			}
			else
			{
				if(  izquierda[i].compareTo( derecha[d] ) > 0)
				{
					lista[o] = izquierda[i];
					i++;
				}
				else
				{
					lista[o] = derecha[d];
					d++;
				}
			}
			o++;
		}
		while( i < izquierda.length )
		{
			lista[o] = izquierda[i];
			o++;
			i++;
		}
		while( d < derecha.length )
		{
			lista[o] = derecha[d];
			o++;
			d++;
		}

		return lista;
	}


}
